set -x

  #LOAD CURRENT RELEASE DIRECTORY
  source /usr/bastion/bbl-dockers/Release_Tags/DeploymentDetails.txt
  
  # Openshift Login
  ${OCP_LOGIN}

  # Docker Login
  ${DOCKER_LOGIN}
  
  #Docker Registry URL
  DOCKER_REGISTRY=${DOCKER_REGISTRY}
    
  #Base Directory
  BASE_DIRECTORY=${HOME_DIRECTORY}
  
  #Release Tag
  RELEASE_TAG=${CURRENT_RELEASE_TAG}
  
  #PP Host Name and Host Ip
  PP_HOST_IP=${PP_HOST_IP}
  PP_HOST_NAME_ONE=${PP_HOST_NAME_ONE}
  PP_HOST_NAME_TWO=${PP_HOST_NAME_TWO}
  
  # OCP Project Name
  PROJECT_NAME=${PROJECT_NAME}
  
  #Docker File Path
  DOCKER_FILE_PATH=${DOCKER_FILE_PATH}
  
 
while read -r line; do


DEPLOYMENT_GROUP="$(echo "$line" | awk '{ print $1 }')"
PRODUCT_NAME="$(echo "$line" | awk '{ print $2 }')"
SERVICES_NAME="$(echo "$line" | awk '{ print $3 }')"
OBJECT_KEY="$(echo "$line" | awk '{ print $4 }')"

  # Deployment Group
  DEPLOYMENT_GROUP="$(echo "$line" | awk '{ print $1 }')"

  # Product Name
  PRODUCT_NAME="$(echo "$line" | awk '{ print $2 }')"

  # Set µservice name
  SERVICE_NAME="$(echo "$line" | awk '{ print $3 }')"

  # Set µservice version
  OBJECT_KEY="$(echo "$line" | awk '{ print $4 }')"

  # Indentifying the jar/tar Name and Version Name from the Object Key
  SERVICE_JAR_NAME="$( echo "$OBJECT_KEY" | awk -F/ '{print $NF}')"
 

  # Since SPA,Node and Java Services Naming Statndard are Different We need to maintain different Spliting Login
  if [ "$DEPLOYMENT_GROUP" == "ui-deployment-configs" ];  then
   SERVICE_VERSION=${SERVICE_JAR_NAME::-4}
   unzip -q ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/${SERVICE_JAR_NAME}  -d ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}
   docker build . -t ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}  -f ${DOCKER_FILE_PATH}/Dockerfile-static-html-component  --build-arg STATIC_CONTENT_DIR==${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/public
   #Deleting the Package
   rm -rf ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/public
   rm -rf ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/public
   
  elif [ "$DEPLOYMENT_GROUP" == "nodejs-deployment-config" ];  then
  
	SERVICE_JAR_VERSION=${SERVICE_JAR_NAME::-7}
	SERVICE_VERSION="$( echo "$SERVICE_JAR_VERSION" | awk -F@ '{print $NF}')"
	# Un tar the Release
    tar -xvf ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/${SERVICE_JAR_NAME} -C ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}
    # Docker Build
    docker build . -t ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}  -f ${DOCKER_FILE_PATH}/Dockerfile-nodejs-component  --build-arg SERVICE_PACK=/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/package
    rm -rf ${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/package
	
  elif [ "$DEPLOYMENT_GROUP" == "java-deployment-config" ];  then
    SERVICE_JAR_VERSION=${SERVICE_JAR_NAME::-4}
	SERVICE_VERSION="$( echo "$SERVICE_JAR_VERSION" | awk -F_ '{print $NF}')"
      if [ "$SERVICE_NAME" == "action-payments" ] || [ "$SERVICE_NAME" == "action-contacts" ] || [ "$SERVICE_NAME" == "action-reports" ] || [ "$SERVICE_NAME" == "action-file-transaction" ] || [ "$SERVICE_NAME" == "digital-audit " ] || [ "$SERVICE_NAME" == "digital-messagedispatcher" ] || [ "$SERVICE_NAME" == "action-icl " ] || [ "$SERVICE_NAME" == "action-sweeps " ] || [ "$SERVICE_NAME" == "digital-messagecenter" ]; then
       docker build . -t ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION} -f ${DOCKER_FILE_PATH}/Dockerfile-java-action-component --build-arg SERVICE_JAR=/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/${SERVICE_JAR_NAME}
	  else
	   docker build . -t ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION} -f ${DOCKER_FILE_PATH}/Dockerfile-java-component --build-arg SERVICE_JAR=/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/${SERVICE_JAR_NAME}
      fi
  fi
  
  
  # Docker Tag Versions
  docker tag ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}  ${DOCKER_REGISTRY}/${SERVICE_NAME}:latest
 
  # Docker Push to Registry
  docker push ${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}
  
  # Deleting Existing Services, Deployments and ConfigMap
  oc delete dc ${SERVICE_NAME}
  oc delete service ${SERVICE_NAME}-service
  oc delete cm ${SERVICE_NAME}-config
  
  # Push config and secrets
      oc new-app --file=${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/configure.yaml \
	  --param=SERVICE_NAME="${SERVICE_NAME}"  \
	  --param=PROJECT_NAME="${PROJECT_NAME}" 
	  
  # Product Processor Dependency Changes Deployments
  if [ "$SERVICE_NAME" == "digital-quest" ] || [ "$SERVICE_NAME" == "ingestion-account-balances" ] || [ "$SERVICE_NAME" == "digital-notifications-dispatcher" ] || [ "$SERVICE_NAME" == "cbx-web" ] || [ "$SERVICE_NAME" == "digital-gazetteer" ] || [ "$SERVICE_NAME" == "action-payments" ] || [ "$SERVICE_NAME" == "action-contacts" ] || [ "$SERVICE_NAME" == "action-reports" ] || [ "$SERVICE_NAME" == "action-file-transaction" ] || [ "$SERVICE_NAME" == "digital-audit" ] || [ "$SERVICE_NAME" == "digital-messagedispatcher" ] || [ "$SERVICE_NAME" == "action-icl" ] || [ "$SERVICE_NAME" == "action-sweeps" ] || [ "$SERVICE_NAME" == "digital-messagecenter" ]; then
      oc new-app --file=${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/deploy.yaml \
	  --param=SERVICE_NAME="${SERVICE_NAME}"  \
	  --param=SERVICE_VERSION="${SERVICE_VERSION}" \
      --param=DOCKER_REGISTRY="docker-registry.default.svc:5000/cbx" \
	  --param=PROJECT_NAME="${PROJECT_NAME}" \
	  --param=BOHOST_IP="${PP_HOST_IP}" \
	  --param=BOHOST_NAME_ONE="${PP_HOST_NAME_ONE}" \
	  --param=BOHOST_NAME_TWO="${PP_HOST_NAME_TWO}" 
  else
      oc new-app --file=${BASE_DIRECTORY}/${RELEASE_TAG}/${DEPLOYMENT_GROUP}/${PRODUCT_NAME}/${SERVICE_NAME}/deploy.yaml \
	  --param=SERVICE_VERSION="${SERVICE_VERSION}" \
      --param=SERVICE_NAME="${SERVICE_NAME}" \
      --param=DOCKER_REGISTRY="docker-registry.default.svc:5000/cbx" 
  fi
      
   
done < ${BASE_DIRECTORY}/${RELEASE_TAG}/${RELEASE_TAG}".txt"

set +x
