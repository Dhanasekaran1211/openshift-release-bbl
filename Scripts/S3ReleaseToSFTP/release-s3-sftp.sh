set -x

# Load the Release tag.txt file
source openshift-release-bbl/CurrentReleaseTag.txt

#Release tag Arg Assinging
RELEASE_TAG=${RELEASE_TAG}

#S3 Commend Bucket Details
S3_BUCKET_DETAILS="s3://cbx-releases"

#GIT DownLoaded Path
GIT_DOWNLOADED_PATH=${HOME_DIR_PATH}

#GIT Details
GIT_CREDENTIALS=${GIT_DETAILS}

#Action Component Declarations
ACTION_COMPONENT=${ACTION_COMPONENT}

#Spliting the Release Tag to Indentify the Release Type
RELEASE_TYPE="$(cut -d'_' -f2 <<<"$RELEASE_TAG")"

echo "$( "${RELEASE_TYPE}" Release DownLoad From S3 Kick Started For the ReleaseTag Version "${RELEASE_TAG}" )" 

# DownLoad the Release From S3

while read -r line; do


DEPLOYMENT_GROUP="$(echo "$line" | awk '{ print $1 }')"
PRODUCT_NAME="$(echo "$line" | awk '{ print $2 }')"
SERVICES_NAME="$(echo "$line" | awk '{ print $3 }')"
OBJECT_KEY="$(echo "$line" | awk '{ print $4 }')"

mkdir -p ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/$DEPLOYMENT_GROUP/$PRODUCT_NAME/$SERVICES_NAME

if [ "$DEPLOYMENT_GROUP" == "node-deployment-jobs" ]; then
 cp  ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/${DEPLOYMENT_CONFIG_FOLDER}/$DEPLOYMENT_GROUP/${PRODUCT_NAME}/${SERVICES_NAME}.yaml   ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/$DEPLOYMENT_GROUP/$PRODUCT_NAME/${SERVICES_NAME}/
 
elif [ "$DEPLOYMENT_GROUP" == "cloud-deployment-config" ]; then
 cd ${RELEASE_TAG}/$DEPLOYMENT_GROUP/$PRODUCT_NAME/$SERVICES_NAME
 git clone  ${GIT_CREDENTIALS}/${OBJECT_KEY}
 git pull
 git checkout master
 cd ${GIT_DOWNLOADED_PATH}
else 
# Required Deploy and Config Yaml File Copying and Moving into RELEASETAG ZIP
 cp ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/${DEPLOYMENT_CONFIG_FOLDER}/$DEPLOYMENT_GROUP/${PRODUCT_NAME}/${SERVICES_NAME}/"deploy.yaml"           ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/$DEPLOYMENT_GROUP/$PRODUCT_NAME/${SERVICES_NAME}/
 cp ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/${DEPLOYMENT_CONFIG_FOLDER}/$DEPLOYMENT_GROUP/${PRODUCT_NAME}/${SERVICES_NAME}/"configure.yaml"        ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/$DEPLOYMENT_GROUP/$PRODUCT_NAME/${SERVICES_NAME}/
#Release Down Load By Passing S3 Object Key and Moving into the Respective Product and Service Names
 aws s3 cp ${S3_BUCKET_DETAILS}/${OBJECT_KEY}     ${RELEASE_TAG}/$DEPLOYMENT_GROUP/${PRODUCT_NAME}/${SERVICES_NAME}/
fi

done < ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/${RELEASE_TAG_FOLDER}/${RELEASE_TAG}".txt"

# These Section will get added only for Fresh Release. Adding the Action Component WebSphere Dependency jar,the Docker Files,ConfigMap and Deployment Shell Scripts
if [ $RELEASE_TYPE == "FULL" ] || [ $RELEASE_TYPE == "FRESH" ]; then 
  mkdir ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/Dependency
  mkdir ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/DockerFile
  cp -r ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/Dependency/WebSphereDependencyJar  ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/Dependency
  cp -r ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/DockerFile/   ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}/DockerFile
fi
cp ${GIT_DOWNLOADED_PATH}/${REPO_FOLDER}/${RELEASE_TAG_FOLDER}/${RELEASE_TAG}".txt"  ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}

zip -r ${RELEASE_TAG}.zip   ${RELEASE_TAG}

echo "$( "${RELEASE_TAG}" Release DownLoad From S3 Completed Successfully for the ReleaseTag Version "${RELEASE_TAG}" )" 

# Moving to Intellect SFTP
echo "$(CBX "${RELEASE_TYPE}"  "${RELEASE_TAG}" movement to SFTP Server Started.)"
sftp c26682@sftp.intellectdesign.com 
put  ${GIT_DOWNLOADED_PATH}/${RELEASE_TAG}.zip  CBXRelease
echo "$(CBX "${RELEASE_TYPE}"  "${RELEASE_TAG}" has been moved to SFTP Server. Please DownLoad the and Proceed with the Deployments.)"

set +x