apiVersion: v1
kind: Template
labels:
  template: java-services-template
message: |-
  The following service(s) have been created in your project: ${PROJECT_NAME}.

      Service: ${SERVICE_NAME} version ${SERVICE_VERSION}

  For more information about using this template, including OpenShift considerations, see SOME_LINK_TO_IGTB_PUBLIC_DOCS.
metadata:
  name: ingestion-reportsmetadata-configuration-template
  annotations:
    description: |-
      Deployment descriptors for ingestion-reportsmetadata-configuration-template, no persistent storage. For more information about using this template, including OpenShift considerations, see SOME_LINK_TO_IGTB_PUBLIC_DOCS.

      NOTE: Some limitations.

objects:
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      project: ${PROJECT_NAME}
    name: ${SERVICE_NAME}-service
  spec:
    ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: 8089
    selector:
      name: ${SERVICE_NAME}-deployment
    type: ${SERVICE_TYPE}

- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      project: ${PROJECT_NAME}
    name: ${SERVICE_NAME}
  spec:
    replicas: 1
    template:
      metadata:
        labels:
          name: ${SERVICE_NAME}-deployment
      spec:
        containers:
        - env:
          - name: JAVA_OPTS
            valueFrom:
              configMapKeyRef:
                key: jvm-options
                name: ${SERVICE_NAME}-config
          - name: logging.level.
            valueFrom:
              configMapKeyRef:
                key: logging-level
                name: ${SERVICE_NAME}-config
           
          # Registry Service Bindings
          - name: REGISTRY_HOSTNAME
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: registry-service-bindings-config
          - name: REGISTRY_PORT
            valueFrom:
              configMapKeyRef:
                key: port
                name: registry-service-bindings-config
          - name: REGISTRY_PROTOCOL
            valueFrom:
              configMapKeyRef:
                key: protocol
                name: registry-service-bindings-config
          - name: REGISTRY_USERNAME
            valueFrom:
              secretKeyRef:
                key: username
                name: registry-service-bindings-secrets
          - name: REGISTRY_PASSWORD
            valueFrom:
              secretKeyRef:
                key: password
                name: registry-service-bindings-secrets
          
          # Eureka Configs
          - name: eureka.instance.preferIpAddress
            valueFrom:
              configMapKeyRef:
                key: eureka-instance-preferIpAddress 
                name: ${SERVICE_NAME}-config  
          - name: eureka.client.enabled
            valueFrom:
              configMapKeyRef:
                key: eureka-client-enabled
                name: ${SERVICE_NAME}-config                       
          
          # Elasticsearch Service Bindings
          - name: ELASTICSEARCH_HOSTNAME
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_PORT
            valueFrom:
              configMapKeyRef:
                key: port
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_PROTOCOL
            valueFrom:
              configMapKeyRef:
                key: protocol
                name: elasticsearch-service-bindings-config
          - name: ELASTICSEARCH_USERNAME
            valueFrom:
              secretKeyRef:
                key: username
                name: elasticsearch-service-bindings-secrets
          - name: ELASTICSEARCH_PASSWORD
            valueFrom:
              secretKeyRef:
                key: password
                name: elasticsearch-service-bindings-secrets      
         
        # Kafka Configs
          - name: KAFKA_HOSTNAME
            valueFrom:
              configMapKeyRef:
                key: hostname
                name: kafka-service-bindings-config
          - name: KAFKA_PORT
            valueFrom:
              configMapKeyRef:
                key: port
                name: kafka-service-bindings-config
          - name: KAFKA_PROTOCOL
            valueFrom:
              configMapKeyRef:
                key: protocol
                name: kafka-service-bindings-config
          - name: KAFKA_USERNAME
            valueFrom:
              secretKeyRef:
                key: username
                name: kafka-service-bindings-secrets
          - name: KAFKA_PASSWORD
            valueFrom:
              secretKeyRef:
                key: password
                name: kafka-service-bindings-secrets       
                   
          # Spring Application
          - name: spring.application.name
            valueFrom:
              configMapKeyRef:
                key: spring-application-name
                name: ${SERVICE_NAME}-config
          - name: spring.cloud.config.name
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-name
                name: ${SERVICE_NAME}-config      
          
          - name: spring.cloud.config.discovery.serviceId
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-discovery-serviceId 
                name: ${SERVICE_NAME}-config      
          - name: spring.cloud.config.label
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-label
                name: ${SERVICE_NAME}-config
          - name: spring.cloud.config.fail-fast
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-fail-fast
                name: ${SERVICE_NAME}-config                                   
          - name: spring.cloud.config.discovery.enabled
            valueFrom:
              configMapKeyRef:
                key: spring-cloud-config-discovery-enabled 
                name: ${SERVICE_NAME}-config     
          image: "${DOCKER_REGISTRY}/${SERVICE_NAME}:${SERVICE_VERSION}"
          name: ${SERVICE_NAME}-java-service
          ports:
            - containerPort: 8089
          imagePullPolicy: Always
          readinessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 8089
            initialDelaySeconds: 120
            timeoutSeconds: 5
          livenessProbe:
            initialDelaySeconds: 30
            tcpSocket:
              port: 8089
            initialDelaySeconds: 120
            timeoutSeconds: 5
            failureThreshold: 10
parameters:
- description: Openshift project to be used.
  displayName: openshift project
  name: PROJECT_NAME
  required: false
- description: Docker REGISTRY URI (igtb).
  displayName: Docker REGISTRY URI
  name: DOCKER_REGISTRY
  required: false
- description: Name of service to be used
  displayName: Name of service to be used
  name: SERVICE_NAME
- description: Version of SERVICE image to be used (1.0, 1.1, 1.2, or latest).
  displayName: Version of SERVICE Image
  name: SERVICE_VERSION
  required: false
- description: Openshift Service Type (clusterIP , NodePortor).
  displayName: Openshift Service Type
  name: SERVICE_TYPE
  required: false

