#!/bin/bash

#NGINX_CONF_FILE=./ui-deployment-configs/cbx-webapp-nginx.conf
NGINX_CONF_FILE=/etc/nginx/conf.d/cbx-webapp-nginx.conf
STATIC_CONTENT_DIR=/usr/share/nginx/html

# Configure security headers
for key in INTELLECT_INQUIRY_API_URI INTELLECT_ACTION_API_URI INTELLECT_PERSISTENCE_INQUIRY_URI INTELLECT_ACCEPTED_DOMAINS OIDC_ISSUER INTELLECT_CF_ASSET_DOMAIN INTELLECT_CF_IMAGE_DOMAIN INTELLECT_CF_VIDEO_DOMAIN INTELLECT_KNOWLEDGE_CENTER_URL; do
    value=$(echo $(eval echo "\$$key"))
    if [ ! "$value" = "dummy" ]; then
        sed -i "s#$key#$value#g" $NGINX_CONF_FILE
        if [ $? -eq 0 ]; then
            echo "INFO - SetSecurityHeaders: replaced placeholder $key --> $value..."
        else
            echo "ERROR - SetSecurityHeaders: could not replace placeholder $key --> $value, exit process!"
            exit 1
        fi
    else
        echo "ERROR - SetSecurityHeaders: environment variable $key has no value assigned, exit process!"
        exit 1
    fi
done

# Configure CBX webapp
for entry in INTELLECT OIDC; do
    for key in $(env | grep ${entry}_ | awk -F= '{ print $1}'); do
        value=$(echo $(eval echo "\$$key"))
        if [ ! "$value" = "dummy" ]; then
            sed -i "s#$key#$value#g" $STATIC_CONTENT_DIR/main.*.js
            if [ $? -eq 0 ]; then
                echo "INFO - AngularAppConfig: replaced placeholder $key --> $value..."
            else
                echo "ERROR - AngularAppConfig: could not replace placeholder $key --> $value, exit process!"
                exit 1
            fi
        else
           echo "ERROR - AngularAppConfig: environment variable $key has no value assigned, exit process!"
           exit 1
        fi
    done
done

nginx -g 'daemon off;'
