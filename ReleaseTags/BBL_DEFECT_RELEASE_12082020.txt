ui-deployment-configs        digital    cbx-web                             igtb-digital-spa/release_2.3.0/igtb-spa-bbl-2.3.0-rc.6.zip
nodejs-deployment-config     digital    digital-quest                       digital-quest/bbl/bbl-digital-quest@0.17.0-rc.114/bbl-digital-quest@0.17.0-rc.114.tar.gz
nodejs-deployment-config     digital    digital-gazetteer                   Gazetteer/BBL/bbl-digital-gazetteer@0.15.0-rc.15/bbl-digital-gazetteer@0.15.0-rc.15.tar.gz
java-deployment-config       cbxbo      digital-alerts                      digital-alerts/release_0.2.4/digital-alerts-release_0.2.4.jar
java-deployment-config       cbxbo      digital-alertpref                   digital-alertpref/release_0.1.0/digital-alertpref-release_0.1.0.jar
java-deployment-config       cbxbo      digital-approvalwf                  digital-approvalwf/release_0.5.2/digital-approvalwf-release_0.5.2.jar
java-deployment-config       cbxbo      ingestion-messagecenter             ingestion-messagecenter/release_1.0.3/ingestion-messagecenter-release_1.0.3.jar
java-deployment-config       cbxbo      ingestion-limits                    ingestion-limits/release_2.0.1/ingestion-limits-release_2.0.1.jar
java-deployment-config       cbxbo      ingestion-accessprofile             ingestion-accessprofile/release_1.1.3/ingestion-accessprofile-release_1.1.3.jar
java-deployment-config       cbxbo      ingestion-roles                     ingestion-roles/release_1.0.8/ingestion-roles-release_1.0.8.jar
java-deployment-config       cbxbo      ingestion-mfa                       ingestion-mfa/release_1.0.2/ingestion-mfa-release_1.0.2.jar
java-deployment-config       cbxbo      ingestion-corporateprofile          ingestion-Corporateprofile/release_0.0.4/ingestion-Corporateprofile-release_0.0.4.jar
java-deployment-config       cbxbo      ingestion-approvalwf                ingestion-approvalwf/release_2.0.2/ingestion-approvalwf-release_2.0.2.jar
java-deployment-config       cbxbo      ingestion-users                     ingestion-users/release_1.2.9/ingestion-users-release_1.2.9.jar
java-deployment-config       cbxbo      digital-messagecenter               digital-messagecenter/release_0.0.7/digital-messagecenter-release_0.0.7.jar
java-deployment-config       cbxbo      digital-userpref                    digital-userpreferences/release_0.2.1/digital-userpreferences-release_0.2.1.jar
node-deployment-jobs         digital    digital-quest                       npm run es-migration-prime
cloud-deployment-config      digital    properties                          igtbdigital/config-bbl-support.git
