ui-deployment-configs        digital    cbx-web                             igtb-digital-spa/release_2.2.0/igtb-spa-bbl-2.2.0-rc.4.zip
nodejs-deployment-config     digital    digital-quest                       digital-quest/bbl/bbl-digital-quest@0.17.0-rc.28/bbl-digital-quest@0.17.0-rc.28.tar.gz
nodejs-deployment-config     digital    digital-reports-generator           reports-generator/bbl/bbl-digital-reports-generator@0.1.0-rc.34/bbl-digital-reports-generator@0.1.0-rc.34.tar.gz
java-deployment-config       ipsh       ingestion-paymentinstruction        ingestion-paymentinstruction/release_1.1.6/ingestion-paymentinstruction-release_1.1.6.jar
java-deployment-config       digital    digital-conductor                   digital-conductor/release_0.0.53/conductor-server-0.0.53-all.jar
java-deployment-config       digital    digital-filestore                   digital-filestore/release_1.1.3/digital-filestore-release_1.1.3.jar
cloud-deployment-config      digital    properties                          igtbdigital/config-irelease.git


