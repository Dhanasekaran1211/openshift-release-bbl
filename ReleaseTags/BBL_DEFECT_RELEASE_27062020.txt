ui-deployment-configs        digital    cbx-web                             igtb-digital-spa/release_2.2.1/igtb-spa-bbl-2.2.1-rc.15.zip
nodejs-deployment-config     digital    digital-quest                       digital-quest/bbl/bbl-digital-quest@0.17.0-rc.65/bbl-digital-quest@0.17.0-rc.65.tar.gz
nodejs-deployment-config     digital    digital-notifications-dispatcher    notification-dispatcher/bbl/digital-notifications-dispatcher@0.2.26/digital-notifications-dispatcher@0.2.26.tar.gz
java-deployment-config       cbxbo      ingestion-users                     ingestion-users/release_1.2.8/ingestion-users-release_1.2.8.jar
java-deployment-config       ipsh       ingestion-txn-payments              ingestion-txn-payments/release_1.0.65/ingestion-txn-payments-release_1.0.65.jar
node-deployment-jobs         digital    digital-quest                       npm run es-migration-prime
node-deployment-jobs         digital    digital-notifications-dispatcher    npm run es-prime
cloud-deployment-config      digital    properties                          igtbdigital/config-bbl-support.git
striim-deployment-config     lqdy       streaming-liquidity-sweeps	        streaming-liquidity-sweeps/release_1.3.23/streaming-liquidity-sweeps-release_1.3.23.jar






