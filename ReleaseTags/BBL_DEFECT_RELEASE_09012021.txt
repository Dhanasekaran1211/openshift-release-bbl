ui-deployment-configs        digital    cbx-web                             igtb-digital-spa/release_2.5.0/igtb-spa-bbl-2.5.0-rc.114.zip
nodejs-deployment-config     digital    digital-quest                       digital-quest/bbl/bbl-digital-quest@0.17.0-rc.214/bbl-digital-quest@0.17.0-rc.214.tar.gz
node-deployment-jobs         digital    digital-quest                       npm run es-updateBaseRates-prime
java-deployment-config       lqdy       ingestion-liquidity-sweeps          ingestion-liquidity-sweeps/release_3.0.19/ingestion-liquidity-sweeps-release_3.0.19.jar
java-deployment-config       digital    action-file-transaction             action-file-transaction/release_1.0.29/action-file-transaction-release_1.0.29.jar
cloud-deployment-config      digital    properties                          igtbdigital/config-bbl-support.git