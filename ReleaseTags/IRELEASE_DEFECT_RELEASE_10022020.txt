ui-deployment-configs        digital    cbx-web                             igtb-digital-spa/release_2.1.0/igtb-spa-bbl-2.1.0-rc.22.zip
nodejs-deployment-config     digital    digital-quest                       digital-quest/bbl/bbl-digital-quest@0.14.26/bbl-digital-quest@0.14.26.tar.gz
nodejs-deployment-config     digital    digital-gazetteer                   Gazetteer/BBL/bbl-digital-gazetteer@0.14.3/bbl-digital-gazetteer@0.14.3.tar.gz
nodejs-deployment-config     digital    ingestion-account-balances          inquiry-ingestion/bbl/ingestion-account-balances-txns-bbl@1.4.31/ingestion-account-balances-txns-bbl@1.4.31.tar.gz
java-deployment-config       lqdy       action-sweeps                       action-sweeps/release_1.2.2/action-sweeps-release_1.2.2.jar
java-deployment-config       ipsh       ingestion-paymentinstruction        ingestion-paymentinstruction/release_1.0.56/ingestion-paymentinstruction-release_1.0.56.jar
java-deployment-config       digital    digital-conductor                   digital-conductor/release_0.0.47/conductor-server-0.0.47-all.jar
java-deployment-config       digital    digital-filestore                   digital-filestore/release_0.1.52/digital-filestore-release_0.1.52.jar
java-deployment-config       digital    action-file-transaction             action-file-transaction/release_1.0.26/action-file-transaction-release_1.0.26.jar
node-deployment-jobs         digital    digital-gazetteer                   npm run es-prime-masterdata
cloud-deployment-config      digital    properties                          igtbdigital/config-irelease.git
