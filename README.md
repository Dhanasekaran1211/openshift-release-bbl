# CBX Application Full(fresh) and Increamental (defect fixes) Release to BBL and Deployments 
 
   Robin has Created all the installation documentations and release deployments for K1. BBL customer release movement to SFTP and deployment approach has been 
   taken from Robin [Base tag](https://bitbucket.org/igtbdigital/openshift-origin-aws/src/master/README.md) and enriched.
     
  This takes you to the complete automated single click fresh/incremental deployments with no manual changes and 
  application works with the all the functionality without any deployment issues in the applications. 
  
   BBL Micro Services list [CBX Release](https://igtbworks.atlassian.net/wiki/spaces/API/pages/1338933509/CBX+BBL+Releases)

  Here we go with the detailed information along with the fully automated release package,shiftment and deployment...
           
## iRelease Certified Release Tag(BBL_RELEASE_TYPE_DDMMYYYY) Process and Definition

   Post successfull certification of BBL Customer Product Release in iRelease envirnonment, 
   Release page[CD page](https://igtbworks.atlassian.net/wiki/spaces/IGTBCBXREL/pages/1222313190/CBX+BBL+Support+10Jan2020) gets published with micro services version and with S3 key. 
   It contains Release Tag(BBL_RELEASE_TYPE_DDMMYYYY) for each releases with BBL_RELEASE_TYPE_DDMMYYYY.txt file on it. It has 
   standard template. In the CD Page CBX Micro Services Release are categoried based on the products functionality easy to group,deploy,indentify the impacted functionality products,test and track.
   In Each and every BBL Customer Release along with the CD page updates Release tag(BBL_RELEASE_TYPE_DDMMYYYY.txt) file gets pushed and  the Same get pushed to [GIT-Repo](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/ReleaseTag/)
   under Release Tags. 
      
   Each Release has this Release tag file in the CD Page. Here is the specification of this tags. In the File Name it has 
   RELEASE_TYPE can have three possible values(DEFECT_RELEASE,FULL_RELEASE,FRESH_RELEASE). Based on this indentifications required
   release itesm gets packed and moved to SFTP with the Name of BBL_RELEASE_TYPE_DDMMYYYY.Zip(RELEASE_TAG.zip)
   
   
   This text file has four section on it to defined the release,
     
       (i)   Deployment Category (7 posssible values are declared below) 
       (ii)  Product 
       (iii) Service Name
       (iv)  S3 Key of the Release.
       
       
   There are 7 type of deployment category is there, each has different steps of deployments instructions.
   
      (i)   UI Deployment (ui-deployment-configs)
      (ii)  NodeJs Deployments (nodejs-deployment-config)
      (iii) Java Component (java-deployment-config)
      (iv)  Java Action Component( Dependency with PP) (java-deployment-config)
      (vi)  Striim Streaming Jar's (striim-deployment-config)
       (v)  Could Config Properties Files (cloud-deployment-config)
       (vi) Prime Jobs (node-deployment-jobs)
             
## Effective Usage of Services Bindings and Properties files

   Simple theroy of effective service binding is that static values are refered from cloud config server(digital-config). 
   Dynamic values are refered from services bindings. CBX Micro Services communicate with the Services(Third Party,Product Processor) based on the requirements. 
   Each micro services has it's own properties file and it gets refered from cloud config server(digital-config).
   Instead of updating the all the Envirnonment(services) values in properties files, services binding concept introduced to avoid the
   updating the properties for dynamic values in every new releases or increamental releases. Properties can be blinded pushed to the 
   cloud config server(GIT). All the Micro Services Deployment yaml configurations are modified to meet this Services Bindings and commited.
   Here is the link [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/ServiceBindings/)  [Sevice Binding Confluence Link](https://igtbworks.atlassian.net/wiki/spaces/API/pages/1338802731/Services+Bindings)
   All the Microservices Deployments yaml files are created with Third Party and Product Processor Bindings.[DeploymentConfig](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/ServiceBindings/)
  
# Release Tag Identification and Release Movement to Onsite Via SFTP

   There are multiple approaches to shift the releases from S3 to SFTP. For Now we have choosed this approach to pack and shift the release SFTP post review with the robin
   approach may get changed. We have used Shell Scripts which has aws cli command to download from S3, to pack and move the Release to SFTP.
    
  Full/Defect/Fresh release indentification happens based on the release tag name and nessary release details published to
  the SFTP.
           
  Services Version gets indetified in S3 key's itself by spliting the S3 key objects. 
  
   This repo has all 6 Folder structures on it,
   
   Dependency Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/Dependency/WebSphereDependencyJar/)
       
        It has external dependency JAR on it.(Action Component Required WebSphere Dependency Jar, This Dependency Jar get's packed with the release only for Fresh Release Type).
        
   DeploymentsConfigurations Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/DeploymentsConfigurations/)
   
      It has all the deployment and config Yaml files here with the Structure. Product Name then Services Name and deploy&config yaml files, It gets packed for every release based on the Services Names in Release Tag)
      
   DockerFile Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/DockerFile/)
    
     It has docker files and start.sh scripts for all the deployment category.
     
   ReleaseTags Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/ReleaseTags/)
     
      It has all the iRelease Certified BBL Releases Tags files here.
      
   ServiceBindings Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/ServiceBindings/)
    
     It has Services Bidnings types and files.( It gets packed only for fresh release types)
     
   CurrentReleaseTag.txt [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/CurrentReleaseTag.txt)
    
     Here We need to Specify the Release tag Name which Releases you need to pack and push to SFTP.
   
   Scripts Folder [RepoLink](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/Scripts/)
   
     It has two scripts one is to pack the release based on the release tags and another scripts to deploy the release in envirnonment.
   
   ## (ii) Script Executions to Shift the Release to SFTP
    
   We need to fill few details to trigger our release movment to SFTP. In CurrentReleaseTag.txt [CurrentReleaseTag](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/CurrentReleaseTag.txt) file Please fill the which release tags we need to shift to SFTP.
   and fill the GIT details of ur's( to clone properties from GIT, if the release tags cloud-deployment-config category)
   
   Here We go with the release pack movement to SFTP based on the Release Type.
   
       git clone https://Dhanasekaran1211@bitbucket.org/Dhanasekaran1211/openshift-release-bbl.git
       cd openshift-release-bbl
       git pull
       cd ..
       
          
  From the Base directory where we have cloned run the below comment. This shell scripts, based on the release tag it indentify the full/increamental
  release and push the complete full(fresh) releases to SFTP with the name of BBL_RELEASETYPE_DDMMYYYY.zip
   
        #Run the Shell Scripts to Prepare a Release and Push it to SFTP
        
          source openshift-release-bbl/Scripts/S3ReleaseToSFTP/release-s3-sftp.sh
               
        # Mac
        
           source openshift-release-bbl/Scripts/S3ReleaseToSFTP/release-s3-sftp.bash
           
  ### Release Package Structure to SFTP
    
    RELEASE_TAG.ZIP
      DEPLOYMENT_CATEGORY(UI,NODE,JAVA)
        PRODUCT_NAME
          SERVICE_NAME
            deploy.yaml
            config.yaml
      DEPLOYMENT_CATEGORY(Cloud-Config)
        PRODUCT_NAME
          *.properties
      DEPLOYMENT_CATEGORY(nodejs-jobs)
        PRODUCT_NAME
          job_tyope.yaml
      DOCKER_FILE       #ONLY FOR FRESH RELEASE THIS GETS PACKED ALONG WITH RELEASE
      DEPENDENCY_JAR    #ONLY FOR FRESH RELEASE THIS GETS PACKED ALONG WITH RELEASE
      SCRIPTS           #ONLY FOR FRESH RELEASE THIS GETS PACKED ALONG WITH RELEASE
      SERVICE_BINDINGS  #ONLY FOR FRESH RELEASE THIS GETS PACKED ALONG WITH RELEASE
      
      RELEASE_TAG.TXT -- Current Release Tag Micro Services List
           
   -- Release shiftment completed to SFTP for the given release tags.
   
  Here is the Shell Scripts Which pack the releases and move it to SFTP.[Release Pack To SFTP](https://bitbucket.org/Dhanasekaran1211/openshift-release-bbl/src/master/Scripts/S3ReleaseToSFTP/release-s3-sftp.bash)
  
## Automatic Pipeline Trigger Based on Release Tag Commit
    
   Now we need to download this repo and fill the current release tag then we need to run the shell scripts. We can make this process automated using bitbucketpipe/yaml
   When ever Release gets cerified they needs to commit the Release_tag.txt file in ReleaseTag folder and they need to change repo envinonment values of Current Release Tag.
   This approach implementation pending. 
     
    -- WIP
    
  
# CBX Full/Defect/Fresh Release Deployment

## Pre requestes
  
    (i)Open Shift Cluster  
    (ii)docker registry
    (iii)Third Party Software
    (iv)Product Processor
       set up has to be in place and running. 
       
 Please follow the order to make sure we acheive the sigle click deployment with to get cerified release end to end working application.
 Execute below from commend master node server(where open shift installed),to download the release from SFTP. We can extend this 
 based on customer flexibility, Instead of downloading into the Master node server, we can download this release from GIT Server and push with the same tag name or 
 We can create a Pipeline(jenkinjobs) in open shift to download fron SFTP and push it to GIT. 
 
 As like how we have packed the release based on the release tag, in the same manner based on the release tags deployments also happens in single click.
 Now manaul changes required.
 
       #To DownLoad the Release From Intellect SFTP Execute below commend

        sftp�c26682@sftp.intellectdesign.com
        get BBL_RELEASETYPE_DDMMYYYY.zip   <<TO FOLDER>>
        
       # Go to The Release Downloaded folder 
        
         cd <<DOWNLOADED_FOLDER>>
         unzip BBL_RELEASETYPE_DDMMYYYY.zip
         cd BBL_RELEASETYPE_DDMMYYYY  
         
  Outside Release tag Folder, this file needs to be updated, where we have Release tag, docker and open shift details.
  Below details needs to be filled for first time alone,
   
     # Where the Release tag gets Downloaded and unzipped.
        HOME_DIRECTORY= 
     # Docker File Base Path where we have shiftes the docker files folder.
        DOCKER_FILE_PATH=
       
     # OC and Docker Logins
        OCP_LOGIN=
        DOCKER_LOGIN=
        
     # Docker Registry, Product Name and Product Processor Host Name and IP. All the Back end Dependent(Product Processor) deploy yaml added with the
       host aliases and ip address as a envirnonments variable as part of deployment this values gets passed as params.
       DOCKER_REGISTRY=
       PROJECT_NAME=
       PP_HOST_IP=
       PP_HOST_NAME_ONE=
     
## Properties Files Push to GIT Repo(Cloud Config Server)
  
   As per the prerequestes third party software has be in place. login the GIT server and Push the Properties files as it's
   
   
     # Coping the Properties file From Release tag and Push it to GIT Repo
     
      BBL_RELEASETYPE_DDMMYYYY/Propertiesfile/ to  <<GIT-SERVER>>/<<REPO>>
      
     # Login with GIT Server and Add, Commit and push the Properties
     
       cd /<<GIT_REPO>>
       git add .
       git commit -am "CBX Application Properties File Initial Version/Defect Releases Commit."
       git push origin master
       
         
## Micro Services Deployments(WEB,NODEJS,JAVA(Action,Other) Components

     # Update the Current Release Tag Which you are going to deploy.
        CURRENT_RELEASE_TAG=
   
    source <BBL_RELEASETYPE_DDMMYYYY>/Scripts/docker-build-deploy.sh
    
   
   Yes, You have Successfully Completed the CBX Deployments by Single Click.Now All the Micro Services will be up and running.
  
  
 
## Services Binding Configuration Deployments For Fresh Release
     
   This Services Binding Deployments required for fresh CBX deployment on New Envirnonments.  
   To Kick Start the Deployments We have to login in Master Node Where we have downloaded the ReleaseTag(BBL_RELEASETYPE_DDMMYYYY)
 
   Step 1: Fill the Third Party Envirnonment Variables in   <BBL_RELEASETYPE_DDMMYYYY>/ServiceBindings/thirdparty-services-details.txt
   Step 2: Fill the Product Processor Envirnonment Varibales in  <BBL_RELEASETYPE_DDMMYYYY>/ServiceBindings/product-processor-details.txt
    
    
    # Docker and Open shift Login to commence the deployments
    source <BBL_RELEASETYPE_DDMMYYYY>/Scripts/OCP/DockerOcpLogin.sh
    
    
    # Load the Envirnonment Values To Replace the Values in Services Binding Configurations
    
    source <BBL_RELEASETYPE_DDMMYYYY>/ServiceBindings/thirdparty-services-details.txt
    source <BBL_RELEASETYPE_DDMMYYYY>/ServiceBindings/product-processor-details.txt
    
    # Run the Shell Scripts to Deploy Third Party and Product Processor Service Bindings
    
    source <BBL_RELEASETYPE_DDMMYYYY>/Scripts/DeployBindingServices/deploy-binding-services.sh
    
    
   Successfully Services Binding Configurations are Deployed in Open Shift. 
    
    
    
    
   
 
    
   
    
  
     
   